package com.inpglobal.bpm.SpringBootRestTM1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan
@EnableAutoConfiguration
public class RestApplicationFirst {
    public static void main(String[] args) {
        SpringApplication.run(RestApplicationFirst.class, args);
    }
}

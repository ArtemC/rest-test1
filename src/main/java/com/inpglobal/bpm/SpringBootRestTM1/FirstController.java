package com.inpglobal.bpm.SpringBootRestTM1;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FirstController {
    private static final String MSG_TEMPLATE = "Message from First Rest test application: %s";

    @RequestMapping("/greetings")
    public ResponseEntity sendGreetings(
            @RequestParam(name = "message", required = false, defaultValue = "default message"
            ) String message) {
        return ResponseEntity.status(200).body(String.format(MSG_TEMPLATE, message));
    }
}

package com.inpglobal.bpm.SpringBootRestTM1;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class GreetingsFirst {
    private String message;
}
